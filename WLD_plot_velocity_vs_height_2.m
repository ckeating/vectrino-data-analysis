function WLD_plot_velocity_vs_height_2(input_filename, num_cells, xlsx_range)
    close all
    % example usage
    % plot_velocity_vs_height_2('../vectrino_data_collection_positions.xlsx', 9, 'A2:F98')
    
    %fprintf('input file name: %s', input_filename);
    % load data from .xlsx file
    [~,~, raw_data] = xlsread(input_filename, xlsx_range);
    
    
    % XYZ coordinate system is relative to the bottom center of the leading
    % (upstream) edge of the vegetation array
    % Positive x is downstream; 
    % Positive y is to left when looking downstream;
    % Positive z is up
    
    % The 4 columns hold x position, y position, z position, and magnitude
    % velocity averaged across cells
    all_avg_velocities = zeros(length(raw_data),4);
    
    
    % get one line of data and plot
    for i = 1:length(raw_data)
        
        file_path = raw_data{i,1};
        x_position =raw_data{i,2};
        y_position = raw_data{i,3};
        z_position = raw_data{i,4};
        start_time = raw_data{i,5};
        end_time = raw_data{i,6};
        
        cleaned_data = open(strcat('../processed_data/',file_path));
        
        avg_velocity_by_measurement = 0;
        for cell_i = 1:num_cells
            times = squeeze(cleaned_data.t{cell_i})./60;

            velocity = squeeze(cleaned_data.v{cell_i});
            velocity_x = velocity(:,1)*100;
            velocity_y = velocity(:,2)*100;
            velocity_z1 = velocity(:,3)*100;
            velocity_z2 = velocity(:,4)*100;
            velocity_mean_z = (velocity_z1+velocity_z2)./2;

            %z_position_updated = z_position + 2 - cell_i/10*4;
            
            start_index = find(times <= start_time, 1, 'last');
            if isempty(start_index)
                start_index=1; 
            end
            end_index = find(times >= end_time, 1, 'first');
            if isempty(end_index)
                end_index=length(times); 
            end
            
            % otherwise you have a divide-by-0 problem below; also 
            % indicates that there is no good data on this time interval
            if start_index == end_index
                break
            end

            avg_x_vel = sum(velocity_x(start_index:end_index)) / ...
                                    (end_index-start_index);
            avg_y_vel = sum(velocity_y(start_index:end_index)) / ...
                                    (end_index-start_index);
            avg_z_vel = sum(velocity_mean_z(start_index:end_index)) / ...
                                    (end_index-start_index);

            magnitude_vel = ...
                sqrt(   avg_x_vel * avg_x_vel + ...
                        avg_y_vel * avg_y_vel + ...
                        avg_z_vel * avg_z_vel       );
            avg_velocity_by_measurement = avg_velocity_by_measurement + magnitude_vel;
            
        end
        
        avg_velocity_by_measurement = avg_velocity_by_measurement/num_cells;
        all_avg_velocities(i,:) = [x_position, y_position, z_position, ...
            avg_velocity_by_measurement];
    end
    
    sorted_avg_velocities = sortrows(all_avg_velocities, [1 3]);
    unique_heights = unique(sorted_avg_velocities(:,3));
    
    % todo: need to split the sorted array into 3 individual arrays and
    % sort each by z; then can just iterate through and sum velocity
    % magnitudes while the z position is constant, then put into another 1D
    % array of avg velocities 
    % plot the vertical position array vs avg velocity array
    % upstream
    

    % make 1D array of avg velocities for each section
    mean_velocities = zeros(length(unique_heights),1);
    
    mean_velocities = calc_mean_velocity_by_height(sorted_avg_velocities, mean_velocities);
    
    function mean_velocities_array = calc_mean_velocity_by_height(sorted_array, mean_velocities_array)
        height_delimiter = sorted_array(1,3);
        n_velocities = 0;
        height_index = 1;
        sum_vels = 0;
        for row_i = 1:length(sorted_array)
            if height_delimiter == sorted_array(row_i,3)
                sum_vels = sum_vels + sorted_array(row_i,4);
                n_velocities = n_velocities + 1;
            else
                mean_velocities_array(height_index) = sum_vels/n_velocities;
                sum_vels = sorted_array(row_i,4);
                height_delimiter = sorted_array(row_i,3);
                height_index = height_index + 1;
                n_velocities = 1;
            end
            mean_velocities_array(height_index) = sum_vels/n_velocities;
        end
    end

    
    figure(1)
    scatter(unique_heights, mean_velocities)
    xlabel('Height Above Bed (cm)')
    ylabel('Mean Velocity (cm/s)')
    title('C2 JUNE')
    hold on

end

    