%   VECTOR_FIELD_PLOTTER_2  3D vector field of velocity magnitudes
%
%   Example:
%
%       vector_field_plotter_2('../vectrino_data_collection_positions.xlsx', 'test_030118',9,'A2:F121')
    
function vector_field_plotter_2(input_filename, to_save_filename, ncells, xlsx_range, varargin)
    close all
    
    if isempty(varargin)
        cell_size_mm = 4;
    else
        cell_size_mm = varargin{1};
    end
    
    fprintf('input file name: %s', input_filename);
    % load data from .xlsx file
    [~,~, raw_data] = xlsread(input_filename, xlsx_range);
    
    
    % XYZ coordinate system is relative to the bottom center of the leading
    % (upstream) edge of the vegetation array
    % Positive x is downstream; 
    % Positive y is to left when looking downstream;
    % Positive z is up
    
    
    figure(1)
    xlim([-150,300])
    ylim([-30,30])
    zlim([0,45])
    hold on
    
    % get one line of data and plot
    for line_i = 1:size(raw_data,1)
        
        file_path = raw_data{line_i,1};
        x_position =raw_data{line_i,2};
        y_position = raw_data{line_i,3};
        z_position = raw_data{line_i,4};
        start_time = raw_data{line_i,5};
        end_time = raw_data{line_i,6};
        full_file_path = strcat('../processed_data/' ,file_path);
        cleaned_data = open(full_file_path);
        
        
        for cell_i = 1:ncells
            times = squeeze(cleaned_data.t{cell_i})./60;

            velocity = squeeze(cleaned_data.v{cell_i});
            velocity_x = velocity(:,1)*100;
            velocity_y = velocity(:,2)*100;
            velocity_z1 = velocity(:,3)*100;
            velocity_z2 = velocity(:,4)*100;
            velocity_mean_z = (velocity_z1+velocity_z2)./2;

            z_position_updated = z_position - 4 - (cell_i-1)/10*cell_size_mm;
            
            start_index = find(times <= start_time, 1, 'last');
            if isempty(start_index)
                start_index=1; 
            end
            end_index = find(times >= end_time, 1, 'first');
            if isempty(end_index)
                end_index=length(times); 
            end

            % otherwise you have a divide-by-0 problem below; also 
            % indicates that there is no good data on this time interval
            if start_index == end_index
                break
            end
            
            avg_x_vel = sum(velocity_x(start_index:end_index)) / ...
                                    (end_index-start_index);
            avg_y_vel = sum(velocity_y(start_index:end_index)) / ...
                                    (end_index-start_index);
            avg_z_vel = sum(velocity_mean_z(start_index:end_index)) / ...
                                    (end_index-start_index);
            % grey out the lowest three velocity vectors from each profile
            %   since they appear shorter at every depth (suspect bad data)
            if cell_i > ncells-3
                color = [0.8 0.8 0.8];  % grey
            else
                color = 'b'; % blue    
            end
            
            % create figure
            figure(1)
            h1 = quiver3(x_position, y_position, z_position_updated, ...
                avg_x_vel, avg_y_vel, avg_z_vel, 'color', color);
            set(h1,'AutoScale','on', 'AutoScaleFactor', 1.0);
            hold on
        end
    end

    figure(1)
    xlabel('Length (cm)')   % Distance downstream from upstream edge of test section 
    ylabel('Width (cm)')    % Distance from middle
    zlabel('Height (cm)')   % Height above bed
    grid on
    
    % save figure
    figure(1)
    
    savefig(to_save_filename)
    
    % animate plot and save as movie
    prompt = char('\n\nmake movie? (y/n) \n');
    user_response = input(prompt,'s');
    if user_response == 'y'
        v = VideoWriter(char(to_save_filename), 'MPEG-4');
        open(v);
        for az = 0 : .5 : 360
            view(az, 40) % view(azimuth, elevation)
            drawnow
            frame=getframe(gcf);
            writeVideo(v,frame);
        end
    end
