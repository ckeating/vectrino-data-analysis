% PLOT_VELOCITY_VS_HEIGHT_2 Velocity-height plots based on vectrino data.
% 
%   Compatible with Wax Lake Delta or Ecogeomorphology Flume profile data.
% 
%   PLOT_VELOCITY_VS_HEIGHT_2(input_filename, num_cells, xlsx_range, cells_to_exclude)
%   scatter plot of mean flow velocity vs. height
%
%   input_filename: the .xlsx file specifying the collection time and 
%   collection positions of each profile.
%
%   num_cells: For Wax Lake Delta, use num_cells=17.  For laboratory flume, 
%   use num_cells=9.
%
%   xlsx_range: The range of data to read from the .xlsx file 
%   (e.g. A2:F121).
%
%   cells_to_exclude: exludes a certain number of lower cells from
%   calculation of mean velocity; it was noticed in the laboratory flume
%   vectrino data that the 3 lowest cells generally tail off to 
%   smaller magnitudes and should be considered unreliable.
%
%   Example:
%
%       plot_velocity_vs_height_2('../vectrino_data_collection_positions.xlsx', 9, 'A2:F121', 3)
%

function [mean_velocities_upstream, mean_velocities_within_veg, ...
    mean_velocities_downstream] = ...
    plot_velocity_vs_height_2(input_filename, num_cells, xlsx_range, cells_to_exclude)
    
    close all
    
    % load data from .xlsx file
    [~,~, raw_data] = xlsread(input_filename, xlsx_range);
    
    % XYZ coordinate system is relative to the bottom center of the leading
    % (upstream) edge of the vegetation array
    % Positive x is downstream; 
    % Positive y is to left when looking downstream;
    % Positive z is up
    
    % The 4 columns hold x position, y position, z position, and magnitude
    %   velocity averaged across cells
    all_avg_velocities = zeros(length(raw_data),4);
    
    
    % loop through one line of data at a time
    for i = 1:length(raw_data)
        
        file_path = raw_data{i,1};
        x_position =raw_data{i,2};
        y_position = raw_data{i,3};
        z_position = raw_data{i,4};
        start_time = raw_data{i,5};
        end_time = raw_data{i,6};
        
        cleaned_data = open(strcat('../processed_data/',file_path));
        
        sum_avg_velocity = 0;
        % loop through each cell in the data file
        for cell_i = 1:num_cells-cells_to_exclude
            times = squeeze(cleaned_data.t{cell_i})./60;

            velocity = squeeze(cleaned_data.v{cell_i});
            velocity_x = velocity(:,1)*100;
            velocity_y = velocity(:,2)*100;
            velocity_z1 = velocity(:,3)*100;
            velocity_z2 = velocity(:,4)*100;
            velocity_mean_z = (velocity_z1+velocity_z2)./2;

            %z_position_updated = z_position + 2 - cell_i/10*4;
            
            start_index = find(times <= start_time, 1, 'last');
            if isempty(start_index)
                start_index=1; 
            end
            end_index = find(times >= end_time, 1, 'first');
            if isempty(end_index)
                end_index=length(times); 
            end
            
            % otherwise you have a divide-by-0 problem below; also 
            % indicates that there is no good data on this time interval
            if start_index == end_index
                break
            end

            avg_x_vel = sum(velocity_x(start_index:end_index)) / ...
                                    (end_index-start_index);
            avg_y_vel = sum(velocity_y(start_index:end_index)) / ...
                                    (end_index-start_index);
            avg_z_vel = sum(velocity_mean_z(start_index:end_index)) / ...
                                    (end_index-start_index);

            magnitude_vel = ...
                sqrt(   avg_x_vel * avg_x_vel + ...
                        avg_y_vel * avg_y_vel + ...
                        avg_z_vel * avg_z_vel       );
            sum_avg_velocity = sum_avg_velocity + magnitude_vel;
            
        end
        
        mean_avg_velocity = sum_avg_velocity/(num_cells-cells_to_exclude);
        all_avg_velocities(i,:) = [x_position, y_position, z_position, ...
            mean_avg_velocity];
    end
    
    sorted_avg_velocities = sortrows(all_avg_velocities, [1 3]);
    
    % find row index of last upstream x position and last within_veg x_position 
    last_upstream_index = find(sorted_avg_velocities(:,1) <= 0, 1, 'last');
    first_within_veg_index = find(sorted_avg_velocities(:,1) > 0, 1, 'first');
    last_within_veg_index = find(sorted_avg_velocities(:,1) <= 200, 1, 'last');
    first_downstream_index = find(sorted_avg_velocities(:,1) > 200, 1, 'first');
    
    % create vector with unique heights grouped by section
    unique_upstream_heights = unique(sorted_avg_velocities(1:last_upstream_index,3));
    unique_within_veg_heights = unique(sorted_avg_velocities(first_within_veg_index:last_within_veg_index,3));
    unique_downstream_heights = unique(sorted_avg_velocities(first_downstream_index:end,3));

    
    % Split the sorted array into 3 individual arrays.
    % Sort each by z; then iterate through and sum velocity magnitudes 
    % while the z position is constant, then put into another 1D
    % array of avg velocities. 
    % Plot the vertical position array vs avg velocity array.
    
    % Split the sorted array into 3 individual arrays.
    sorted_avg_velocities_upstream = sorted_avg_velocities(1:last_upstream_index,:);
    sorted_avg_velocities_within_veg = sorted_avg_velocities(first_within_veg_index:last_within_veg_index,:);
    sorted_avg_velocities_downstream = sorted_avg_velocities(first_downstream_index:end,:);

    % Sort each by z.
    sorted_z_upstream = sortrows(sorted_avg_velocities_upstream, 3);
    sorted_z_within_veg = sortrows(sorted_avg_velocities_within_veg, 3);
    sorted_z_downstream = sortrows(sorted_avg_velocities_downstream, 3);

    % Make 1D array of avg velocities for each section.
    mean_velocities_upstream = zeros(length(unique_upstream_heights),1);
    mean_velocities_within_veg = zeros(length(unique_within_veg_heights),1);
    mean_velocities_downstream = zeros(length(unique_downstream_heights),1);
    
    % Checks below are intended to handle WLD field data, which has no
    % upstream/downstream information.
    if ~isempty(mean_velocities_upstream)
        mean_velocities_upstream = calc_mean_velocity_by_height(sorted_z_upstream, mean_velocities_upstream);
    end
    if ~isempty(mean_velocities_within_veg)
        mean_velocities_within_veg = calc_mean_velocity_by_height(sorted_z_within_veg, mean_velocities_within_veg);
    end
    if ~isempty(mean_velocities_downstream)
        mean_velocities_downstream = calc_mean_velocity_by_height(sorted_z_downstream, mean_velocities_downstream);
    end
    
    
    % Returns array of mean velocities by distance above channel bed for a
    % particular channel section (e.g. upstream, within-vegetation, or
    % downstream)
    function mean_velocities_array = calc_mean_velocity_by_height(sorted_array, mean_velocities_array)
        height_delimiter = sorted_array(1,3);
        n_velocities = 0;
        height_index = 1;
        sum_vels = 0;
        for row_i = 1:size(sorted_array,1)
            if height_delimiter == sorted_array(row_i,3)
                sum_vels = sum_vels + sorted_array(row_i,4);
                n_velocities = n_velocities + 1;
            else
                mean_velocities_array(height_index) = sum_vels/n_velocities;
                sum_vels = sorted_array(row_i,4);
                height_delimiter = sorted_array(row_i,3);
                height_index = height_index + 1;
                n_velocities = 1;
            end
            mean_velocities_array(height_index) = sum_vels/n_velocities;
        end
    end

    %% Plotting
    figure(1)
    if ~isempty(mean_velocities_upstream)
        subplot(1,3,3)
        scatter(mean_velocities_upstream, unique_upstream_heights)
        ylabel('Height above bed (cm)')
        xlabel('Mean Velocity (cm/s)')
        title('Upstream')
        hold on
    end
    if ~isempty(mean_velocities_within_veg)
        subplot(1,3,2)
        scatter(mean_velocities_within_veg, unique_within_veg_heights)
        ylabel('Height above bed (cm)')
        xlabel('Mean Velocity (cm/s)')
        title('Within Vegetation')
    end
    if ~isempty(mean_velocities_downstream)
        subplot(1,3,1)
        scatter(mean_velocities_downstream, unique_downstream_heights)
        ylabel('Height above bed (cm)')
        xlabel('Mean Velocity (cm/s)')
        title('Downstream')
    end
    
    % Add text description
    ax1_text = axes('Position',[0 0 1 1],'Visible','off');
    descr = {   sprintf('Cells excluded from calculation of mean velocities: %d', cells_to_exclude)};
    axes(ax1_text) % sets ax1 to current axes
    text(0.425, 0.02, descr)
end