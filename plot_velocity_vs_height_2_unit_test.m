%% Main function to generate tests
function tests = plot_velocity_vs_height_2_unit_test
tests = functiontests(localfunctions);
end

%% Test Functions
function test_WLD_C1_all_cells(testCase)
    [actSolution, ~, ~] = plot_velocity_vs_height_2( ...
        '../WLD_vectrino_data_collection_positions.xlsx', 17, 'A2:F14',0);
    expSolution = [ 6.3998;
                    4.3610;
                    2.6468;
                    1.7727;
                    1.5439;
                    1.6111;
                    1.6890;
                    1.6699;
                    0.3406;
                    0.3322;
                    0.2473;
                    0.0886;
                    8.4758];
    verifyEqual(testCase, actSolution, expSolution, 'RelTol', 1e-2)
end
