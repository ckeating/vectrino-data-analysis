% Calculates settling rate constant and settling velocity based on decrease 
% in scattering strength (amplitude) in Vectrino raw data.  Regression is 
% performed on the interval start_time to end_time (min).  
% Method derived from Palmer et al. 2004
%
% Note: if periods of drastically higher scattering strength are present in
% the plots, it is likely an effect of the the instrument being positioned
% close to the bed for that duration of time.

function amplitude_to_settling_rate(data, start_time, end_time, height_of_water_cm)
    
    close all
    
    % load data
    raw_data = load(data);
    times_sec = raw_data.Data.Profiles_TimeStamp;
    mean_amp_bycell = (raw_data.Data.Profiles_AmpBeam1 + ...
        raw_data.Data.Profiles_AmpBeam2 + ...
        raw_data.Data.Profiles_AmpBeam3 + ...
        raw_data.Data.Profiles_AmpBeam4) ./ 4;
    
    % calculate mean scattering strength across all four directional probes
    mean_SNR_bycell = (raw_data.Data.Profiles_SNRBeam1 + ...
        raw_data.Data.Profiles_SNRBeam2 + ...
        raw_data.Data.Profiles_SNRBeam3 + ...
        raw_data.Data.Profiles_SNRBeam4) ./ 4;
    
    % discard last three cells because their signal strength is much lower,
    % consistent with treating their velocity measurements as bad data.
    % Then calculate the mean scattering strength across remaining six cells.
    mean_SNR_bycell = mean_SNR_bycell(:,1:6);
    mean_SNR_across_allcells = mean(mean_SNR_bycell,2);
    
    mean_amp_bycell = mean_amp_bycell(:,1:6);
    mean_amp_across_allcells = mean(mean_amp_bycell,2);
    
    
    % plot mean SNR vs time
    figure(1)
    subplot(2,1,1)
%     plot(times_sec/60, mean_SNR_across_allcells ./ (mean_SNR_across_allcells(start_time*60)))
    plot(times_sec/60, mean_SNR_across_allcells)

    xlabel('Time (min)')
    ylabel('Scattering strength (dB)')
    title('Data.Profiles\_SNRBeam')
    hold on
    % plot amplitude vs time
    subplot(2,1,2)
%     plot(times_sec/60, mean_amp_across_allcells ./ (mean_amp_across_allcells(start_time*60)))
    plot(times_sec/60, mean_amp_across_allcells)
    xlabel('Time (min)')
    ylabel('Scattering strength (dB)')
    title('Data.Profiles\_AmpBeam')
    % calculate initial sediment concentration based on time of floc/sediment addition 
    % for 02/15/18 experiment, kaolinite was added at t=10 min and was
    % well-mixed by t=15-20 min]
    % time_interval_min = [15 20];
    time_interval_min = [start_time end_time];

    time_interval_sec = time_interval_min * 60;
    lower_index = find(times_sec<=time_interval_sec(1), 1, 'last');
    upper_index = find(times_sec>=time_interval_sec(2), 1, 'first');
    
    % mean_amp_initial_concentration = mean(mean_SNR_across_allcells(lower_index:upper_index));
    
    % Store the times/values on the interval in question separately, for
    % readability later.
    decay_times = times_sec(lower_index:upper_index);
    SNR_vals = mean_SNR_across_allcells(lower_index:upper_index);
    Amp_vals = mean_amp_across_allcells(lower_index:upper_index);
    
    % Linear fit of form y = p1*x + p2
    lin_fit_SNR = fit(decay_times, SNR_vals, 'poly1');
    lin_fit_Amp = fit(decay_times, Amp_vals, 'poly1');
    
    % Compute correlation coefficient of each model
    R_SNR = corrcoef(decay_times, SNR_vals);
    R_Amp = corrcoef(decay_times, Amp_vals);
    
    % Compute R-squared from off-diagonal corrcoef value
    Rsq_SNR = R_SNR(2,1).^2;
    Rsq_Amp = R_Amp(2,1).^2;

    % Plot data and linear model
    figure(2)
    plot(lin_fit_SNR, decay_times, SNR_vals)
    title(sprintf('Linear fit on SNR data, R^2=%d', Rsq_SNR))
    
    figure(3)
    plot(lin_fit_Amp, decay_times, Amp_vals)
    title(sprintf('Linear fit on Amplitude data, R^2=%d', Rsq_Amp))
    
    % Compute settling rate based on Palmer et al. 2004 eq 13 and k = m/(10
    % log(e)) where log() refers to base-10 logarithm (see p79, Palmer et
    % al. 2004).
    settling_rate_k_SNR = lin_fit_SNR.p1 / (10 * log10(exp(1)));
    settling_rate_k_Amp = lin_fit_Amp.p1 / (10 * log10(exp(1)));
    
    sprintf('settling rate k SNR: %d' , settling_rate_k_SNR) 
    sprintf('settling rate k: %d' , settling_rate_k_Amp)
    sprintf('settling velocity with SNR data: %d  cm/sec', settling_rate_k_SNR*(height_of_water_cm)) 
    sprintf('settling velocity with amplitude data: %d  cm/sec', settling_rate_k_Amp*(height_of_water_cm)) 
