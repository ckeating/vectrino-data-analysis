function turbulence_subplotter(x_rows, y_rows, subplot_number, subplot_title, x_data, y_data, color, last_iteration)
    subplot(x_rows, y_rows, subplot_number)
    scatter(x_data, y_data, [], color)
    % cuts execution time roughly in half
    if last_iteration
        xlabel('Turbulence')
        ylabel('Height above bed (cm)')
        title(subplot_title)
    end
    hold on