% PLOT_PROCESSED_DATA
%   
%   Time series plot of mean x y and z velocity for one cell
%   PLOT_PROCESSED_DATA_2(filename) plots a time series of x, y, and
%   z (average of z1 and z2) velocities vs time, where x, y, and z values
%   are from 1 cell in the input data file.
%
%   Example: 
%   plot_processed_data('WLD_C1_JUNE_00000_processed.mat', 5)

function plot_processed_data(filename, cell_no)
    
    close all
    cleaned_data = open(filename);
    
    times = squeeze(cleaned_data.t{cell_no});
    times = times./60.0;
    
    velocity = squeeze(cleaned_data.v{cell_no});
    velocity_x = velocity(:,1)*100;
    velocity_y = velocity(:,2)*100;
    velocity_z1 = velocity(:,3)*100;
    velocity_z2 = velocity(:,4)*100;
    velocity_mean_z = (velocity_z1+velocity_z2)./2;
    
    % general
    figure(cell_no)
    
    % unsmoothed
    subplot(3,1,1)
    
    % add background patches based on location of sensor at various times
%     patch([0 540 540 0], [-0.15 -0.15 0.15 0.15],[0.7 0.7 0.7])
%     patch([600 840 840 600], [-0.15 -0.15 0.15 0.15],[0.7 0.7 0.7])
%     patch([960 1230 1230 960], [-0.15 -0.15 0.15 0.15],[0.7 0.7 0.7])
%     patch([1290 1500 1500 1290], [-0.15 -0.15 0.15 0.15],[0.7 0.7 0.7])
%     patch([1500 1830 1830 1500], [-0.15 -0.15 0.15 0.15],[0.7 0.7 0.7])
%     alpha(0.3)
    
    plot(times, [velocity_x, velocity_y, velocity_mean_z]);
    xlabel('Time (min)')
    ylabel('Velocity (cm/s)')
    ylim([-15, 15]);
    title(sprintf('XYZ Velocity vs Time, Cell %d', cell_no));
    legend('X', 'Y', 'Z\_mean', 'Location','southeast')
    hold on
    
    
    
    
    % smoothed
    subplot(3,1,2)
    smoothed_x = movmean(velocity_x,60);
    smoothed_y = movmean(velocity_y,60);
    smoothed_z = movmean(velocity_mean_z,60);
    figure(cell_no)
    plot(times, [smoothed_x, smoothed_y, smoothed_z]);
    xlabel('Time (min)')
    ylabel('Velocity (cm/s)')
    ylim([-15, 15]);
    title(sprintf('60-Point Smoothed XYZ Velocity vs Time, Cell %d', cell_no));
    legend('X', 'Y', 'Z\_mean', 'Location','southeast')
    
    
    
    
    % very smoothed
    subplot(3,1,3)
    smoothed_x = movmean(velocity_x,200);
    smoothed_y = movmean(velocity_y,200);
    smoothed_z = movmean(velocity_mean_z,200);
    figure(cell_no)
    plot(times, [smoothed_x, smoothed_y, smoothed_z]);
    xlabel('Time (min)')
    ylabel('Velocity (cm/s)')
    ylim([-15, 15]);
    title(sprintf('200-Point Smoothed XYZ Velocity vs Time, Cell %d', cell_no));
    legend('X', 'Y', 'Z\_mean', 'Location','southeast')
    
    
    