function plot_processed_data_turbulence(xls_filename, xls_range, ...
    x_position, y_position, cells_to_grey, experiment_identifier)
    close all
    
    % At one x/y position, plot turbulence vs z position (bunch of subplots
    % for different turbulences)
    
    % get a portion of some data file's values according to
    % data collection time; refer to the xls file
    [~,~, raw_data] = xlsread(xls_filename, xls_range);
    
    % effectively iterating through profile heights here since each line of
    % data represents one 5-minute collection period at a specific height
    
    figure(1)
    for i = 1:size(raw_data,1)
         
        % check if it's the last iteration; if so, add plot title, axis
        % labels (cuts execution time roughly in half by not labelling each
        % iteration)
        last_iteration = false;
        if i==size(raw_data,1)
            last_iteration = true;
        end
        
        rawdata_file_path = raw_data{i,1};
        rawdata_x_position_cm = raw_data{i,2};
        rawdata_y_position_cm = raw_data{i,3};
        rawdata_z_position_cm = raw_data{i,4};
        rawdata_start_time_s = raw_data{i,5};
        rawdata_end_time_s = raw_data{i,6};
        
        if x_position == rawdata_x_position_cm && y_position == rawdata_y_position_cm
            
            cleaned_data = open(strcat('../processed_data/',rawdata_file_path));
            turbulences = cleaned_data.vpvp_full;
            ncells = length(turbulences);
            % stores turbulence values at each cell in order to calculate
            % mean turbulence at each height
            turbulence_by_cell = zeros(4,4,9);
            % iterating through cells here
            for cell_i = 1:ncells
                last = last_iteration & cell_i==ncells;
                
                % from config:  cellSize: 40;  nCells: 9;  cellStart: 400
                %                    (0.1mm),                     (0.1mm)
                % cells start 4cm below the collection position as measured
                % from the bed to the center doppler transducer
                rawdata_z_position_updated = rawdata_z_position_cm - 4 - (cell_i-1)/10*4;
                
                % grey out the last ~3 cells in plots since cells 7-9 in 
                % lab flume data seem to tail off, regardless of profile
                % depth
                if cell_i > ncells-cells_to_grey
                    color = [0.8 0.8 0.8];  % grey
                else
                    color = 'b'; % blue    
                end
                
                times = squeeze(cleaned_data.t{cell_i})./60;
                % get index of start and end times
                start_index = find(times <= rawdata_start_time_s, 1, 'last');
                if isempty(start_index)
                    start_index=1; 
                end
                end_index = find(times >= rawdata_end_time_s, 1, 'first');
                if isempty(end_index)
                    end_index=length(times); 
                end
                
                
                % get turbulence data over entire collection period
                uu = squeeze(turbulences{cell_i}(:,1,1));
                vv = squeeze(turbulences{cell_i}(:,2,2));
                w1w1 = squeeze(turbulences{cell_i}(:,3,3));
                w2w2 = squeeze(turbulences{cell_i}(:,4,4));
                
                uv = squeeze(turbulences{cell_i}(:,1,2));
                uw1 = squeeze(turbulences{cell_i}(:,1,3));
                uw2 = squeeze(turbulences{cell_i}(:,1,4));
                vw1 = squeeze(turbulences{cell_i}(:,2,3));
                vw2 = squeeze(turbulences{cell_i}(:,2,4));
                w1w2 = squeeze(turbulences{cell_i}(:,3,4));
                
                % mean turbulence over collection period
                uu_mean = sum(uu(start_index:end_index)) / ...
                    (end_index-start_index);
                vv_mean = sum(vv(start_index:end_index)) / ...
                    (end_index-start_index);
                w1w1_mean = sum(w1w1(start_index:end_index)) / ...
                    (end_index-start_index);
                w2w2_mean = sum(w2w2(start_index:end_index)) / ...
                    (end_index-start_index);
                
                uv_mean = sum(uv(start_index:end_index)) / ...
                    (end_index-start_index);
                uw1_mean = sum(uw1(start_index:end_index)) / ...
                    (end_index-start_index);
                uw2_mean = sum(uw2(start_index:end_index)) / ...
                    (end_index-start_index);
                vw1_mean = sum(vw1(start_index:end_index)) / ...
                    (end_index-start_index);
                vw2_mean = sum(vw2(start_index:end_index)) / ...
                    (end_index-start_index);
                w1w2_mean = sum(w1w2(start_index:end_index)) / ...
                    (end_index-start_index);
               
                turbulence_subplotter(4, 4, 1, 'UU', uu_mean, rawdata_z_position_updated, color, last)
                turbulence_subplotter(4, 4, 2, 'UV', uv_mean, rawdata_z_position_updated, color, last)
                turbulence_subplotter(4, 4, 3, 'UW1', uw1_mean, rawdata_z_position_updated, color, last)
                turbulence_subplotter(4, 4, 4, 'UW2', uw2_mean, rawdata_z_position_updated, color, last)
                turbulence_subplotter(4, 4, 6, 'VV', vv_mean, rawdata_z_position_updated, color, last)
                turbulence_subplotter(4, 4, 7, 'VW1', vw1_mean, rawdata_z_position_updated, color, last)
                turbulence_subplotter(4, 4, 8, 'VW2', vw2_mean, rawdata_z_position_updated, color, last)
                turbulence_subplotter(4, 4, 11, 'W1W1', w1w1_mean, rawdata_z_position_updated, color, last)
                turbulence_subplotter(4, 4, 12, 'W1W2', w1w2_mean, rawdata_z_position_updated, color, last)
                turbulence_subplotter(4, 4, 16, 'W2W2', w2w2_mean, rawdata_z_position_updated, color, last)

                turbulence_by_cell(:,:,cell_i) = ...
                    [uu_mean,   uv_mean,    uw1_mean,   uw2_mean;
                        0,      vv_mean,    vw1_mean,   vw2_mean;
                        0,          0,      w1w1_mean,  w1w2_mean;
                        0,          0,          0,      w2w2_mean];
            end
        end
    end
    figure(1)
    % add text description
    ax1_text = axes('Position',[0 0 1 1],'Visible','off');
    descr = {   sprintf('Blue Circles = Cells 1-%d',(ncells-cells_to_grey));
                sprintf('Grey Circles = Cells %d-%d',(ncells-cells_to_grey+1),ncells)};
    axes(ax1_text) % sets ax1 to current axes
    text(0.45, 0.05, descr)
    
    % make fullscreen and save as PNG (saving as fig takes an extra 25 seconds)
    set(gcf, 'Position', get(0, 'Screensize'));
    %savefig(sprintf('../code_outputs/turbulence_plots/Turbulence_%s_X=%dcm_Y=%dcm_.fig', experiment_identifier, x_position, y_position))
    saveas(gcf,sprintf('../code_outputs/turbulence_plots/Turbulence_%s_X=%dcm_Y=%dcm_.png', experiment_identifier, x_position, y_position))
    